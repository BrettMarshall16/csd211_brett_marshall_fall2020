/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q2;

/**
 *Author: Brett Marshall
 *Date: 2020-11-08
 *Creates a new person and new car adds a owner to the car 
 */
public class App {
    public void run(){
        
        Person person1 = new Person("Joe", "Student", 23, 0.0,0.0,"M");
        
        Car dodge = new Car("Dodge", "Ram", 2016, person1, 829301);
        
        System.out.println(dodge);
        
    }
}
