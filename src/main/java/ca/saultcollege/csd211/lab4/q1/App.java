/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q1;

/**
 *Author: Brett Marshall
 * Date: 2020-11-08
 * Creates 3 people and also compares
 */
public class App {
    public void run(){
        Person person1 = new Person("Joe", "Student", 23, 0.0,0.0,"M");
        Person person2 = new Person("George", "Student", 24, 188.2,0.0,"M");
        Person person3 = new Person("Mary", "Teacher", 43, 170.0,48.2,"F");
        
        System.out.println(person1);
        System.out.println("-----------");
        System.out.println(person2);
        System.out.println("-----------");
        System.out.println(person3);
        System.out.println("-----------");
        
        //check to see if 2 people are equal.
        if (person1.equals(person2)){
            System.out.println("They are equal");
        }
        
    }
}
