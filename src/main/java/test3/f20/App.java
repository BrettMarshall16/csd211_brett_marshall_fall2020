/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3.f20;

import java.util.Scanner;

/**
 *Author : Brett Marshall
 *Date: 2020-12-07
 * adds,edits and lists courses
 */
public class App {
    private Course[] courseList = new Course[25];
    private int courseCounter = 1; // this is a instance variable
    
    public void run(){
        boolean done=false;
        while(!done){
            Scanner input = new Scanner(System.in);
            System.out.println("1. Add a Course");
            System.out.println("2. Edit a Course");
            System.out.println("3. List Courses");
            System.out.println("99. Exit");
            System.out.println("Enter an option");
            
            int choice=0;
            try{
                choice=input.nextInt();
            }catch(Exception e){
                System.out.println("Problem, try again...");
                continue;
            }        
            switch(choice){
                case 1: addCourse();
                        break;
                case 2: editCourse();
                        break;
                case 3: listCourses();
                        break;
                case 99: done=true;
                        break;
                default: System.out.println("Bad input, try again please.");
            }
        }
    }
    private void addCourse() {
            Scanner input = new Scanner(System.in);
            System.out.println("Enter course name: ");
            String courseName = input.next(); // this is a local variable
            System.out.println("Enter course code: ");
            String courseCode = input.next(); // this is also a local variable
            System.out.println("Enter number of students: ");
            int numberofStudents = input.nextInt();
            System.out.println("Enter start date: ");
            String startDate = input.next();
            Course x = new Course(courseName, courseCode, startDate, numberofStudents); // method paramaters is courseName,courseCode,startDate,NumberofStudents
            courseList[courseCounter + 1] = x;
    }
    
    private void editCourse(){
        listCourses();
        Scanner input = new Scanner(System.in);
        System.out.println("Which course would you like to edit(use index): ");
        int courseIndex = input.nextInt();
        System.out.println("Enter course name: ");
        String courseName = input.next();
        System.out.println("Enter course code: ");
        String courseCode = input.next();
        System.out.println("Enter number of students: ");
        int numberofStudents = input.nextInt();
        System.out.println("Enter start date: ");
        String startDate = input.next();
        courseList[courseIndex].setCourseName(courseName);
        courseList[courseIndex].setCourseCode(courseCode);
        courseList[courseIndex].setStartDate(startDate);
        courseList[courseIndex].setNumberofStudents(numberofStudents);
    }
    
    private void listCourses(){
        for (Course course : courseList)
            if (course == null)
                break;
        int i = 1;
        System.out.println("Course" + " " + i + ":" + courseList[i]);
        i++;
    }
}
