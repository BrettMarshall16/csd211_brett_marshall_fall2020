

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 17052
 */
import ca.saultcollege.csd211.lab4.q4.Car;
import ca.saultcollege.csd211.lab4.q4.App;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CarTests {
    Car car1 = new Car("Ford", "Mustang", 2019, "1234");
    Car car2 = new Car("Ford", "Mustang", 2019, "12345");
    Car car3 = new Car("Ford", "Mustang", 2019, "1234");
    Car car4 = car3;
    
    @Test
    public void test_1() {
        // car an newCar have the same vin so this test should pass
        assertEquals("Testing cars", car1, car4);
    }
    public void test_2(){
        assertNotSame("Testing Cars", car1, car2);
    }
    public void test_3(){
        assertEquals("Testing cars", car1, car3);
    }
    public void test_4(){
        App.testAddCar(new Car("Ford", "Mustang", 2019, "1234l"));
        Car car = new Car("Ford", "Mustang", 2019, "12343l");
        Car foundCar = App.findCar(car);
        assertNull("Testing",foundCar);
    }
}

