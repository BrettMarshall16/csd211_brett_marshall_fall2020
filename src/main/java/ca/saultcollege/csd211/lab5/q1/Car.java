/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab5.q1;



/**
 *
 * @author 17052
 */

public class Car extends Object implements java.io.Serializable {
    String make;
    String model;
    int year;
    String vin;
    Person ownertest = new Person();

  
    public Car(String make, String model, int year, String vin) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin = vin;

    }
    
    public Car(){
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin = vin;
    }

    public String toString(){
        return "Make: " + make + "\n" + "Model: " + model + "\n" + "Year: " + year + "\n" + "Owner: " + ownertest.getFirstName() + " " + ownertest.getLastName() + "\n" + "Vin: " + vin;
    }
    
    // set make
    public void setMake(){
        this.make = make;
    }
    
    //get model
    public void setModel(){
        this.model = model;
    }
    
   public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
    
    public boolean equals(Object o){
        if(! (o instanceof Car))
            return false;
        Car x=(Car)o;
        if(x.vin == this.vin)
                return true;
       return false;
    }
}
