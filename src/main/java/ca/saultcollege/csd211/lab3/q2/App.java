/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab3.q2;

import java.util.Scanner;

/**
 *Author Brett Marshall
 * 2020-10-29
 * Check if a number is valid
 */
public class App {
   
    /**
     *
     * 
     */
    public void run(){
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        String input = " ";
        do{
            System.out.println("Please enter an account number to check: " );
            input = String.valueOf(scanner.nextLong());
            System.out.println("The number you entered is: " + checkAccountNumber(input));
            System.out.println("Would you like to enter another number(true to exit false to do another number)?:");
            exit = scanner.nextBoolean();
        } while(!exit);
    }
    
    public static boolean checkAccountNumber(String input)
        {
                int sum = 0;
                boolean check = false;
                for (int i = input.length() - 1; i >= 0; i--)
                {
                        int n = Integer.parseInt(input.substring(i, i + 1));
                        if (check)
                        {
                                n *= 2;
                                if (n > 9)
                                {
                                    n = (n % 10) + 1;
                                }
                        }
                        sum += n;
                        check = !check;
                }
                return (sum % 10 == 0);
        }

}

