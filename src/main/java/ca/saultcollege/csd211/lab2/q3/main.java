/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab2.q3;

import java.util.Scanner;
import java.text.DecimalFormat;

/**
Author : Brett Marshall
Date : 2020-10=11
Description : f to c
**/
public class main {
    //formats celcius to 2 decimals
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    
    public static void main(String[] args) {
    //declaring 
    double fahrenheit;
    double celcius;
    
    //scanner
    Scanner input = new Scanner(System.in);
    
    //accept values and calculates
    System.out.println("Enter a value in fahrenheit:");
    fahrenheit = input.nextDouble();
    celcius = (fahrenheit - 32) * 5/9;
    
    //formats and prints
    System.out.println("The value in celcius is: "+ df2.format(celcius));
    
    }
}
