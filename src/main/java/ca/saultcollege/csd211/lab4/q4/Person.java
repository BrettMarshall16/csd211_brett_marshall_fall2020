/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q4;

import ca.saultcollege.csd211.lab4.q3.*;


/**
 *
 * @author 17052
 */
public class Person extends Object implements java.io.Serializable {
    //initalize strings.
    
    private int age;
    private String firstName;
    private String lastName;
    private double height;
    private double weight;
    private String gender;
    private int hotDogsSold;
    
    // When used sets the information given to correct varriables
    public Person(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }
    public Person(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }
    
    // toString to print out everything in string. Also nice format
    public String toString(){
        return "Name: " + firstName+ " " + lastName;
    }
    
    // set first name
    public void setFirstName(){
        this.firstName = firstName;
    }
    //set last name
    public void setLastName(){
        this.lastName = lastName;
    }
    // set age
    public void setAge(){
        this.age = age;
    }
}
