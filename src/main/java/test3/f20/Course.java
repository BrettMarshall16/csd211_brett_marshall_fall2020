/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3.f20;

/**
 *
 * @author 17052
 */
public class Course extends Object implements java.io.Serializable{
    private String courseName; // course name
    private String courseCode; // course code
    private String startDate; // start date
    private int numberofStudents; // number of students
    private int count; // counter
    
    public Course(){
        // default constructor
    }
    
    public Course(String courseName, String courseCode, String startDate, int numberofStudents){
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.startDate = startDate;
        this.numberofStudents = numberofStudents;
        count++;
    }
    
    public String toString(){
        return courseCode + ", " + courseName;
    }
    
    @Override
    public boolean equals(Object o){
        if(! (o instanceof Course))
            return false;
        Course x=(Course)o;
        if(x.courseCode == this.courseCode)
                return true;
       return false;
    }
    
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getNumberofStudents() {
        return numberofStudents;
    }

    public void setNumberofStudents(int numberofStudents) {
        this.numberofStudents = numberofStudents;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    
}
