/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q3;

import java.util.Scanner;
import ca.saultcollege.csd211.lab4.q3.Car;
import ca.saultcollege.csd211.lab4.q3.Person;

/**
 *Author: Brett Marshall
 *Date: 2020-11-08
 *Allows you to add cars to an array, edit the cars,add owners,sell cars and list cars.
 */
public class App {

    private Car[] cars = new Car[100];
    private static int carCounter = 0;
    
    public void run() throws Exception{
        String theMenu = "1. Add A Car\n" + "2.Sell A Car\n" +
                         "3.List All Cars\n" + "4.Delete A Car\n" +
                         "5. Edit A Car\n" + "6. Quit";
        boolean quit = false;
        Scanner input = new Scanner(System.in);
        
        while(!quit){
            System.out.println(theMenu);
            int option = input.nextInt();
            
            switch(option){
                case 1:
                    addCar();
                    break;
                case 2:
                    sellCar();
                    break;
                case 3:
                    listCars();
                    break;
                case 4:
                    deleteCar();
                    break;
                case 5:
                    editCar();
                case 6:
                    quit();
            }
            
        }
        
    }
    
    private void addCar() throws Exception{
        Car car = new Car();
        if (carCounter == 100){
            throw new Exception("LOT IS FULL");
        }
        boolean quit = false;
        while (!quit){
            Scanner input = new Scanner(System.in);
            System.out.println("Enter Make: ");
            String make = input.next();
            System.out.println("Enter model: ");
            String model = input.next();
            System.out.println("Enter year: ");
            int year = input.nextInt();
            System.out.println("Enter a VIN: ");
            String vin = input.next();
            
            Car x = new Car(make, model, year, vin);
            cars[carCounter++] = x;
            System.out.println("Would you like to quit true or false");
            quit = input.nextBoolean();
        }
    }
    
    private void sellCar(){
        Person owner = getOwner();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the vin of the car sold!: ");
        String tempvin = input.next();
        for (int i = 0; i < cars.length; i++){
            if (cars[i].vin.equals(tempvin)){
            cars[i].owner = owner;
            break;
            }
        }
    }
    
    private void editCar(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the vin of the car you wish to Edit: ");
        String tempvin = input.next();
        for(int i = 0; i < cars.length; i++){
            if (cars[i].vin.equals(tempvin)){
                System.out.println("Enter the cars new owner: ");
                Person owner = getOwner();
                cars[i].owner = owner;
                break;
            }
        }
    }
    
    private void listCars(){
        for (Car car : cars){
            if (car == null)
                break;
            System.out.println(car);
        }
    }
    
    private void deleteCar(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the vin of the car you wish to delete: ");
        String tempvin = input.next();
        for(int i = 0; i < cars.length; i++){
            if (cars[i].vin.equals(tempvin)){
                cars[i] = null;
                break;
            }
        }
    }
    
    private Person getOwner(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter First Name: ");
        String firstName = input.next();
        System.out.println("Enter Last Name: ");
        String lastName = input.next();
        Person x = new Person(firstName, lastName);
        return x;
    }
    
    private boolean quit(){
        boolean quit = true;
        return quit;
    }
    
}
