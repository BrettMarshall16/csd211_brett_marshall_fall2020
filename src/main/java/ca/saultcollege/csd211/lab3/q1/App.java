/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab3.q1;

import java.util.Scanner;

/**
 *
 * @author 17052
 */
public class App {

    /**
     * Author : Brett Marshall
     * 2020 - 10 - 29
     * Turn a 10 digit number to 11 using luhns
     */
    public void run() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        String input;
        long num;
        do {
            System.out.println("Please enter an account number to check: ");
            num = scanner.nextLong();
            input = Long.toString(num);
            System.out.println("The number you entered is: " + input + checkdigit(input));
            System.out.println("Would you like to enter another number(true to exit false to do another number)?:");
            exit = scanner.nextBoolean();
        } while (!exit);
    }

    public int checkdigit(String input) {
        int sum = 0;
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(input.length() - i - 1);
            int digit = (int) ch - 48;
            if (i % 2 == 0) {
                digit = digit * 2;
            } 
            if ( digit > 9){
                digit -= 9;
            }
            sum += digit;
        }
        return (10 - (sum % 10)) % 10;

    }
}
