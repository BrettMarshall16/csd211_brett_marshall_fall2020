/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab5.q1;

import ca.saultcollege.csd211.lab5.q1.*;


/**
 *
 * @author 17052
 */
public class Person extends Object implements java.io.Serializable {
    //initalize strings.
    
    private int age;
    private String firstName;
    private String lastName;
    private double height;
    private double weight;
    private String gender;
    
    public Person(){
        
    }
    
    // When used sets the information given to correct varriables
    public Person(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }
    
    public Person(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    // toString to print out everything in string. Also nice format
    public String toString(){
        return "Name: " + firstName+ " " + lastName;
    }
    
    // set first name
    public void setFirstName(){
        this.firstName = firstName;
    }
    //set last name
    public void setLastName(){
        this.lastName = lastName;
    }
    // set age
    public void setAge(){
        this.age = age;
    }
}
