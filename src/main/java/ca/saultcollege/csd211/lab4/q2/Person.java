/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q2;

/**
 *
 * @author 17052
 */
public class Person extends Object implements java.io.Serializable {
    //initalize strings.
    
    private final int age;
    private final String firstName;
    private final String lastName;
    private final double height;
    private final double weight;
    private final String gender;
    
    // When used sets the information given to correct varriables
    public Person(String firstName, String lastName, int age, double height, double weight, String gender){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }
    
    // toString to print out everything in string. Also nice format
    public String toString(){
        return "Name: " + firstName+ " " + lastName;
    }
}
