package gradecalc123;

import gradecalc123.Grades3;
import java.util.Scanner;

public class GradeCalculator3 {

    public static void main(String[] args) {
        String frstName;
        String lstName;
        int studID;
        double prjtAverageOne;
        double lbAverageOne;
        double tstAverageOne;
        double exmGradeOne;
        double courseGradeOne;
        double courseLetterGradeOne;
        double courseGradeTwo;
        int numStudents = 0;
        boolean isStudentPassing;

        Grades3 student1 = new Grades3();
        Grades3 student2 = new Grades3();

        displayGreeting();
        System.out.println("------Student 1------");
        frstName = inputFirstName();
        student1.setFirstName(frstName);
        lstName = inputLastName();
        student1.setLastName(lstName);
        studID = inputStudentID();
        student1.setStudentID(studID);
        System.out.println("------Student 2------ ");
        frstName = inputFirstName();
        student2.setFirstName(frstName);
        lstName = inputLastName();
        student2.setLastName(lstName);
        studID = inputStudentID();
        student2.setStudentID(studID);
        System.out.println("------Student 1 Grades------");
        student1Choice(student1);
        System.out.println("------Student 2 Grades------");
        student1Choice(student2);

        courseGradeOne = calculateCourseGrade(student1);
        courseGradeTwo = calculateCourseGrade2(student2);
        displayGrades(student1, courseGradeOne);
        displayGrades2(student2, courseGradeTwo);
        displayThanks();
    }

    public static void displayGreeting() {
        System.out.println("Welcome user to the Grade Calculator.");
    }

    public static void displayThanks() {
        System.out.println("Thank you for using this Grade Calculator.");
    }

    public static String inputFirstName() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input student First Name: ");
        String firstName;
        firstName = keyboard.next();
        return firstName;
    }

    public static String inputLastName() {
        Scanner keyboard = new Scanner(System.in);
        String lastName;
        System.out.println("Input student Last name: ");
        lastName = keyboard.next();
        return lastName;
    }

    public static int inputStudentID() {
        Scanner keyboard = new Scanner(System.in);
        int studentID;
        System.out.println("Input Student ID: ");
        studentID = keyboard.nextInt();
        return studentID;
    }

    public static int displayMenu() {
        Scanner input = new Scanner(System.in);
        int choice;
        double labAverage;
        double projectAverage;
        double testAverage;
        double finalExamGrade;

        System.out.println("0 - Quit");
        System.out.println("1 - Lab");
        System.out.println("2 - Project");
        System.out.println("3 - Test");
        System.out.println("4 - Final Exam");

        choice = input.nextInt();
        return choice;
    }

    public static void student1Choice(Grades3 student1) {
        Scanner input = new Scanner(System.in);
        int choice = displayMenu();
        switch (choice) {
            case 1:
                System.out.println("Input Lab Average: ");
                student1.setlabAverage(input.nextDouble());
                choice = displayMenu();
            case 2:
                System.out.println("Input project average: ");
                student1.setprojectAverage(input.nextDouble());
                choice = displayMenu();
            case 3:
                System.out.println("Input test average: ");
                student1.settestAverage(input.nextDouble());
                choice = displayMenu();
            case 4:
                double finalExamGrade;
                System.out.println("Input final exam grade: ");
                student1.setfinalExamGrade(input.nextDouble());
                choice = displayMenu();
        }
    }

    public static void student2Choice(Grades3 student2) {
        Scanner input = new Scanner(System.in);
        int choice = displayMenu();
        switch (choice) {
            case 1:
                System.out.println("Input Lab Average: ");
                student2.setlabAverage(input.nextDouble());
                choice = displayMenu();
            case 2:
                System.out.println("Input project average: ");
                student2.setprojectAverage(input.nextDouble());
                choice = displayMenu();
            case 3:
                System.out.println("Input test average: ");
                student2.settestAverage(input.nextDouble());
                choice = displayMenu();
            case 4:
                double finalExamGrade;
                System.out.println("Input final exam grade: ");
                student2.setfinalExamGrade(input.nextDouble());
                choice = displayMenu();
        }
    }

    public static double calculateCourseGrade(Grades3 student1) {
        double courseGrade1;
        double labAverage;
        double projectAverage;
        double testAverage;
        double finalExamGrade;
        labAverage = student1.getlabAverage();
        projectAverage = student1.getprojectAverage();
        testAverage = student1.gettestAverage();
        finalExamGrade = student1.getfinalExamGrade();
        courseGrade1 = (0.3 * labAverage) + (0.3 * testAverage + 0.1) + (.3 * projectAverage) + (0.1 * finalExamGrade);
        return courseGrade1;
    }

    public static double calculateCourseGrade2(Grades3 student2) {
        double courseGrade2;
        double labAverage;
        double projectAverage;
        double testAverage;
        double finalExamGrade;
        labAverage = student2.getlabAverage();
        projectAverage = student2.getprojectAverage();
        testAverage = student2.gettestAverage();
        finalExamGrade = student2.getfinalExamGrade();
        courseGrade2 = (0.3 * labAverage) + (0.3 * testAverage + 0.1) + (.3 * projectAverage) + (0.1 * finalExamGrade);
        return courseGrade2;
    }

    public static void displayGrades(Grades3 student1, double courseGradeOne) {
        boolean isStudentPassing;
        isStudentPassing = (courseGradeOne >= 59.5);

        System.out.println("");
        System.out.println("");
        System.out.println("Student's Name: " + student1.getFirstName() + " " + student1.getLastName());
        System.out.println("Student ID: " + student1.getStudentID());
        System.out.println("Lab Average:          " + student1.getlabAverage());
        System.out.println("Project Average:      " + student1.getprojectAverage());
        System.out.println("Test Average:         " + student1.gettestAverage());
        System.out.println("Final Exam Grade:     " + student1.getfinalExamGrade());
        System.out.println("Course Grade:         " + courseGradeOne);
        System.out.println("Is this student passing?(true/false): " + isStudentPassing);
    }

    public static void displayGrades2(Grades3 student2, double courseGradeTwo) {
        boolean isStudentPassing;
        isStudentPassing = (courseGradeTwo >= 59.5);

        System.out.println("");
        System.out.println("");
        System.out.println("Student's Name: " + student2.getFirstName() + " " + student2.getLastName());
        System.out.println("Student ID: " + student2.getStudentID());
        System.out.println("Lab Average:        " + student2.getlabAverage());
        System.out.println("Project Average:    " + student2.getprojectAverage());
        System.out.println("Test Average:       " + student2.gettestAverage());
        System.out.println("Final Exam Grade:   " + student2.getfinalExamGrade());
        System.out.println("Course Grade:       " + courseGradeTwo);
        System.out.println("Is this student passing?(true/false): " + isStudentPassing);
    }

}
