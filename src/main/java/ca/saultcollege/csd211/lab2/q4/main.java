/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab2.q4;

import java.util.Scanner;

/**
Author : Brett Marshall
Date : 2020-10=11
Description : volume of a cylinder
**/
public class main {
    public static void main(String[] args) {
        
    //variables
    double radius;
    double length;
    double area;
    double volume;
        
    //scanner
    Scanner input = new Scanner(System.in);
        
    // accept data
    System.out.println("Enter the radius of the cylinder:");
    radius = input.nextDouble();
    System.out.println("Enter the length of the cylinder:");
    length = input.nextDouble();
    
    //calculations
    
    area = radius * radius * 3.14159265359;
    volume = area * length;
    
    // print
    
    System.out.println("A cylinder with a radius of " + radius + "and a length of " + length + "has a volume of " + volume );
    
    }
}
