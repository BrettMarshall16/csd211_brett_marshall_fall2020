/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q1;

/**
 *
 * @author 17052
 */
public class Car extends Object implements java.io.Serializable {
    private String make;
    private String model;
    private int year;
    private String owner;
    private int vin;
    
    public Car(String make, String model, int year, String owner, int vin){
        this.make = make;
        this.model = model;
        this.year = year;
        this.owner = owner;
        this.vin = vin;
    }
    public String toString(){
        return "Make: " + make + "\n" + "Model: " + model + "\n" + "Year: " + year + "\n" + "Owner: " + owner + "\n" + "Vin: " + vin;
    }
}
