/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q3;



/**
 *
 * @author 17052
 */
public class Car extends Object implements java.io.Serializable {
    String make;
    String model;
    int year;
    Person owner;
    String vin;
    
    public Car(){
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin = vin;
    }

    public Car(String make, String model, int year, String vin) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin = vin;
    }

    public String toString(){
        return "Make: " + make + "\n" + "Model: " + model + "\n" + "Year: " + year + "\n" + "Owner: " + owner + "\n" + "Vin: " + vin;
    }
    
    // set make
    public void setMake(){
        this.make = make;
    }
    
    //get model
    public void setModel(){
        this.model = model;
    }
    
    public boolean equals(Object o){
        if(! (o instanceof Car))
            return false;
        Car x=(Car)o;
        if(x.vin == this.vin)
                return true;
       return false;
    }
}
