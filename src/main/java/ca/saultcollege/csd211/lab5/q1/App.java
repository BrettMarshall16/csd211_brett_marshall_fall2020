/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab5.q1;

import ca.saultcollege.csd211.lab5.q1.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Author: Brett Marshall
 *Date: 2020-12-02
 *Allows you to add cars to an array, edit the cars,add owners,sell cars and list cars.
 */
public class App {
    ArrayList<Car> cars = new ArrayList<Car>();
    private static int carCounter = 0;
    
    public void run() throws Exception{
        String theMenu = "1. Add A Car\n" + "2.Sell A Car\n" +
                         "3.List All Cars\n" + "4.Delete A Car\n" +
                         "5. Edit A Car\n" + "6. Quit\n" + "7. Save Cars";
        boolean quit = false;
        Scanner input = new Scanner(System.in);
        
        while(!quit){
            System.out.println(theMenu);
            int option = input.nextInt();
            
            switch(option){
                case 1:
                    addCar();
                    break;
                case 2:
                    sellCar();
                    break;
                case 3:
                    listCars();
                    break;
                case 4:
                    deleteCar();
                    break;
                case 5:
                    editCar();
                case 6:
                    quit();
                case 7:
                    saveCars();
            }
            
        }
        
    }
    
    public void addCar() throws Exception{
        if (carCounter == 100){
            throw new Exception("LOT IS FULL");
        }
        boolean quit = false;
        while (!quit){
            Scanner input = new Scanner(System.in);
            System.out.println("Enter Make: ");
            String make = input.next();
            System.out.println("Enter model: ");
            String model = input.next();
            System.out.println("Enter year: ");
            int year = input.nextInt();
            System.out.println("Enter a VIN: ");
            String vin = input.next();
            Car x = new Car(make,model,year,vin);
            cars.add(x);
            carCounter++;
            System.out.println("Would you like to quit true or false");
            quit = input.nextBoolean();
        }
    }
    
    public void sellCar(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the vin of the car sold!: ");
        String tempvin = input.next();
        System.out.println("Enter first name: ");
        String tempfirst = input.next();
        System.out.println("Enter last name: ");
        String templast = input.next();
        for (int i = 0; i < cars.size(); i++){
            if (cars.get(i).getVin().equals(tempvin)){
                cars.get(i).ownertest.setFirstName(tempfirst);
                cars.get(i).ownertest.setLastName(templast);
                break;
                
            }
        }
    }
    
    public void editCar(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the vin of the car you wish to Edit: ");
        String tempvin = input.next();
        System.out.println("Enter first name: ");
        String tempfirst = input.next();
        System.out.println("Enter last name: ");
        String templast = input.next();
        
        for(int i = 0; i < cars.size(); i++){
            if (cars.get(i).getVin().equals(tempvin)){
                System.out.println("Enter the cars new owner: ");
                cars.get(i).ownertest.setFirstName(tempvin);
                cars.get(i).ownertest.setLastName(templast);
                break;
            }
        }
    }
    
    public void listCars(){
        for (Car car : cars){
            System.out.println(car);
        }
    }
    
    private void deleteCar(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the vin of the car you wish to delete: ");
        String tempvin = input.next();
        for(int i = 0; i < cars.size(); i++){
            if (cars.get(i).getVin().equals(tempvin)){
                cars.remove(i);
                break;
            }
        }
    }
    
    public Person getOwner(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter First Name: ");
        String firstName = input.next();
        System.out.println("Enter Last Name: ");
        String lastName = input.next();
        Person x = new Person(firstName, lastName);
        return x;
    }
    
    public boolean quit(){
        boolean quit = true;
        return quit;
    }
    public void saveCars(){
        try {
            FileOutputStream saveFile = new FileOutputStream("test.sav");
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(cars);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
