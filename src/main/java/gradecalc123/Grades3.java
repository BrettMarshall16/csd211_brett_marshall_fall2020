package gradecalc123;

public class Grades3
        
{
   public String firstName;
   public String lastName;
   public int studentID;
   public double labAverage = 0;
   public double projectAverage = 0;
   public double testAverage = 0;
   public double finalExamGrade = 0;
   public static int numStudents = 0;
   
   
   
   
   public void setFirstName(String frstName)
   {
      firstName = frstName;
   }
   public String getFirstName()
   {
      return firstName;
   }
   
   
   public void setLastName(String lstName)
   {
      lastName = lstName;
   }
   public String getLastName()
   {
      return lastName;
   }
   
   
   public void setStudentID(int studID)
   {
      studentID = studID;
   }
   public int getStudentID()
   {
      return studentID;
   }
   
   
   public void setprojectAverage(double prjtAverage)
   {
      projectAverage = prjtAverage;
   }
   public double getprojectAverage()
   {
      return projectAverage;
   }
   
   
   public void setlabAverage(double lbAverage)
   {
      labAverage = lbAverage;
   }  
   public double getlabAverage()
   {
      return labAverage;
   }
 
   
   public void setfinalExamGrade(double exmGrade)
   {
      finalExamGrade = exmGrade;
   }
   public double getfinalExamGrade()
   {
      return finalExamGrade;
   }
 
   
   public void settestAverage(double tstAverage)
   {
      testAverage = tstAverage;
   }
   public double gettestAverage()
   {
      return testAverage;
   }
   
 
 
 
 
 
}