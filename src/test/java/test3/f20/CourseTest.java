/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3.f20;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import test3.f20.Course;
/**
 *
 * @author 17052
 */
public class CourseTest {
    private Course course1 = new Course("Math", "CSD20002", "April 2020", 10);
    private Course course2 = new Course("Math", "CSD20002", "April 2020", 10);
    private Course course3 = new Course("Java", "CSD1000", "April 2020", 10);
    private Course course4 = new Course("Python", "CSD2000", "April 2020", 10);
    
    @Test
     public void testCourse1() {
         assertTrue("Does course 1 equal course 2: ", course1.equals(course2));
     }
    @Test
    public void testCourse2(){
        assertFalse("Does course 3 equal course 4: ", course3.equals(course4));
    }
}
