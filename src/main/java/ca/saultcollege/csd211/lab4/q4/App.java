/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q4;

import java.util.Scanner;
import ca.saultcollege.csd211.lab4.q4.Car;
import ca.saultcollege.csd211.lab4.q4.Person;

/**
 *Author: Brett Marshall
 *Date: 2020-11-08
 *Allows you to add cars to an array, edit the cars,add owners,sell cars and list cars.
 */
public class App {

    static Car[] cars = new Car[100];
    static int carCounter = 0;
    
    public static void testAddCar(Car x){
        cars[carCounter]= x;
        carCounter++;
     }
    
     public static void testDeleteCar(Car Car){
         cars[carCounter++]=null;
     }
     
    public static Car findCar(Car x){
        for(Car c:cars){
            if(c==null) 
                return null;
            if(c.equals(x))
                return c;
        }
        return null;
    }
}
