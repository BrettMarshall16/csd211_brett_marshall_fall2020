/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q2;


/**
 *
 * @author 17052
 */
public class Car extends Object implements java.io.Serializable {
    private final String make;
    private final String model;
    private final int year;
    private final Person owner;
    private final int vin;
    
    public Car(String make, String model, int year, Person person, int vin){
        this.make = make;
        this.model = model;
        this.year = year;
        this.owner = person;
        this.vin = vin;
    }
    public String toString(){
        return "Make: " + make + "\n" + "Model: " + model + "\n" + "Year: " + year + "\n" + "Owner: " + owner + "\n" + "Vin: " + vin;
    }
}
