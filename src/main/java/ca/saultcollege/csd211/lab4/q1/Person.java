/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab4.q1;

/**
 *
 * @author 17052
 */
public class Person extends Object implements java.io.Serializable {
    //initalize strings.
    
    private final int age;
    private final String firstName;
    private final String lastName;
    private final double height;
    private final double weight;
    private final String gender;
    
    // When used sets the information given to correct varriables
    public Person(String firstName, String lastName, int age, double height, double weight, String gender){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }
    
    // toString to print out everything in string. Also nice format
    public String toString(){
        return "Name: " + firstName+ " " + lastName + "\n" + "Age : " + age + "\n" + "Height: " + height + "\n" + "Weight: " + weight + "\n" + "Gender: " + gender;
    }
    
    // Method to check to see if the object passed is equal to another.
    public boolean equals(Object o){
        //if o is not passed an instance person return false
        if(! (o instanceof Person))
            return false;
        // set object to person 
        Person p=(Person)o;
        // if statement thats checks to see if first name and last name are the same
        if(p.firstName == this.firstName)
            if(p.lastName == this.lastName)
                return true;
       return false;
    }
    
}
