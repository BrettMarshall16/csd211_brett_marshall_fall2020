/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd211.lab2.q1;

import java.util.Scanner;

/**
Author : Brett Marshall
Date : 2020-10=11
Description : prints different values then asks user for a different set
**/

public class Main {

    public static void main(String[] args) {
        // initalize and declare variables 
        byte a = 1;
        char b = 'a';
        short c = 1;
        int d = 1;
        long e = 1;
        float f = 1;
        double g = 1;
        boolean h = true;
        String i = "hello";

        //print out variables already set
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(g);
        System.out.println(h);
        System.out.println(i);
        System.out.println("hello2");

        Scanner input = new Scanner(System.in);
        System.out.println("Enter a byte:");
        a = input.nextByte();
        System.out.println(a);

        System.out.println("Enter a char:");
        b = input.next().charAt(0);
        System.out.println(b);
        
        System.out.println("Enter a short:");
        c = input.nextShort();
        System.out.println(c);
        
        System.out.println("Enter an int:");
        d = input.nextInt();
        System.out.println(d);
        
        System.out.println("Enter a long:");
        e = input.nextLong();
        System.out.println(e);
        
        System.out.println("Enter a float:");
        f = input.nextFloat();
        System.out.println(f);
        
        System.out.println("Enter a double:");
        g = input.nextDouble();
        System.out.println(g);
        
        System.out.println("Enter False:");
        h = input.nextBoolean();
        System.out.println(h);
        
        System.out.println("Enter a word:");
        i = input.next();
        System.out.println(i);

    }
}
